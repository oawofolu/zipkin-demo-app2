package io.vmware.pubsec.distributedtracing.app2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class App2 {

	public static void main(String[] args) {
		SpringApplication.run(App2.class, args);
	}

}

@RestController
class HelloWorldSampleB {
	
	private static Logger LOG = LoggerFactory.getLogger(HelloWorldSampleB.class);
	
	@GetMapping("/")
	public String main() {
		LOG.info("In Service B...");
		return "Hello World! From Service B";
	}
}
